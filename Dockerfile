FROM nginx

ENV WEBSOCKET_SERVER="127.0.0.1:32401"

COPY nginx_domain.conf /etc/nginx/conf.d/default.conf
COPY ./web/ /usr/share/nginx/html/web
COPY ./socket/ /usr/share/nginx/html/socket
COPY ./startup.sh ./

RUN chmod +x startup.sh

RUN apt update && apt upgrade -y
RUN apt install -y \
  php  \
  php-cli \
  unzip \
  supervisor

RUN cd /; curl -sS https://getcomposer.org/installer -o composer-setup.php;
RUN HASH=`curl -sS https://composer.github.io/installer.sig`; \
  php -r "if (hash_file('SHA384', 'composer-setup.php') === '$HASH') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;";

RUN php composer-setup.php --install-dir=/usr/local/bin --filename=composer;

RUN cd /usr/share/nginx/html/socket; \
  composer install; \
  composer dumpautoload -o;

RUN mkdir -p /etc/supervisor/conf.d
COPY ./supervisord.conf /etc/supervisor/conf.d/

CMD ["/startup.sh"]
