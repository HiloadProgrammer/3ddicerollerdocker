# Major's 3D Dice Docker Repo

This is a fork of https://github.com/MajorVictory/3DDiceRoller

This repo servs the original Major's 3D Dice Server as an docker container

<a href="https://www.flaticon.com/free-icons/dice" title="dice icons">Dice icons created by Freepik - Flaticon</a>

# Usage

To build the container use
```
docker build --force-rm -t dice-roller .
```

And to start the container use and replace "<--YOUR-IP-->" with ip you want to
bind the service to
```
docker run --name dice -p <--YOUR-IP-->:8080:80 -p <--YOUR-IP-->:32400:32400 -e WEBSOCKET_SERVER="<--YOUR-IP-->:32400" dice-roller
```

If you want to use rooms feature, you have to set the "WEBSOCKET_SERVER"
enviroment variable.

# Original README

Major's 3D Dice

Uses three.js and cannon.js to simulate actual dice and read their results.
http://threejs.org
http://cannonjs.org

Original code based from Tealyatina's dice roller
http://www.teall.info/2014/01/online-3d-dice-roller.html

Heavily modified, rewritten, and extended to add textures, dice, and color sets.
Try the live version here: http://dnd.majorsplace.com/dice/
Alternate github pages version: https://majorvictory.github.io/3DDiceRoller/
Server code for the online portion can be found here: https://github.com/MajorVictory/3DDiceWebsocket

Star Wars™ RPG font from The Alexandrian
http://thealexandrian.net/wordpress/37660/roleplaying-games/star-wars-force-and-destiny-system-cheat-sheet

Star Wars™ Armada font from err404
https://boardgamegeek.com/filepage/116568/armada-icon-fonts

Star Wars™ X-Wing font from geordanr
https://github.com/geordanr/xwing-miniatures-font

Star Wars™ Legion font from lyerelian
https://github.com/lyerelian/Legion-font

autodice.php contains an example of using the JavaScript PostMessage API from another tab to trigger dice throws.
