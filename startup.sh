#!/bin/bash

echo "WEBSOCKET_SERVER '$WEBSOCKET_SERVER'" > /myenv.txt

if [ -z ${WEBSOCKET_SERVER+x} ]; then
  WEBSOCKET_SERVER="127.0.0.1:32400"
fi

echo "WEBSOCKET_SERVER '$WEBSOCKET_SERVER'" >> /myenv.txt

sed -i 's/<--YOUR_SOCKET_SERVER-->/'${WEBSOCKET_SERVER}'/g' /usr/share/nginx/html/web/includes/DiceRoller.js
sed -i 's/<--YOUR_SOCKET_SERVER-->/'${WEBSOCKET_SERVER}'/g' /usr/share/nginx/html/web/includes/Teal.js

exec /usr/bin/supervisord -n -c /etc/supervisor/conf.d/supervisord.conf
